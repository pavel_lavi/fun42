import subprocess


def execute_cmd(cmd) -> bool:
    try:
        process = subprocess.Popen([cmd],
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE,
                                   shell=True)
        stdout, stderr = process.communicate()
    except Exception as ex:
        raise Exception(f"failed to run scenario, cmd: {cmd}, stderr: {stderr}, error: {ex}")

    if not stderr.decode('utf-8') and process.returncode in [0, 1]:
        return not process.returncode

    raise Exception(f"scenario execution failed, cmd: {cmd}. error: {stderr}")