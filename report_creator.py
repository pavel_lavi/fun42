import csv
from constants import Constants
from rates_calc import RatesDto


class ReportCreator:
    _csv_file = "report.csv"
    _rates = RatesDto

    def __init__(self, rates_dto):
        self._rates = rates_dto

    def _csv_dict_writer(self, report_file, dict):
        dict_writer = csv.DictWriter(report_file, fieldnames=dict.keys())
        dict_writer.writeheader()
        dict_writer.writerows([dict])

    def _csv_nested_dict_writer(self, report_file, dict):
        def mergedict(a, b):
            a.update(b)
            return a

        fields = Constants.program_versions.copy()
        fields.insert(0, "version")
        dict_writer = csv.DictWriter(report_file, fieldnames=fields)
        dict_writer.writeheader()
        for k, d in sorted(dict.items()):
            dict_writer.writerow(mergedict({'version': k}, d))

    def _csv_write_dict_section(self, str_writer, title, dict_writer):
        str_writer.writerow([title])
        dict_writer()
        str_writer.writerow(["\n"])

    def create_report(self):
        with open(self._csv_file, mode='a') as report_file:
            try:
                str_writer = csv.writer(report_file, delimiter='.')
                # todo: task requirement 1 - The success rate for each possible combination of program version and windows versions (for the “check” and “install_plugin” function)
                self._csv_write_dict_section(
                    str_writer,
                    "Check operations success rate",
                    lambda: self._csv_nested_dict_writer(report_file, self._rates.check_rates))
                self._csv_write_dict_section(
                    str_writer,
                    "Install plugin operations success rate",
                    lambda: self._csv_nested_dict_writer(report_file, self._rates.install_plugin_rates))

                # todo: task requirement 2 - The success rate of each windows version (for all program versions) and the success rate for each program version (for all windows versions)
                self._csv_write_dict_section(
                    str_writer,
                    "Check operations success rate per windows version",
                    lambda: self._csv_dict_writer(report_file, self._rates.check_per_windows))
                self._csv_write_dict_section(
                    str_writer,
                    "Check operations success rate per program version ",
                    lambda: self._csv_dict_writer(report_file, self._rates.check_per_program))
                self._csv_write_dict_section(
                    str_writer,
                    "Install plugin operations success rate per windows version",
                    lambda: self._csv_dict_writer(report_file, self._rates.install_plugin_per_windows))
                self._csv_write_dict_section(
                    str_writer,
                    "Install plugin operations success rate per program version",
                    lambda: self._csv_dict_writer(report_file, self._rates.install_plugin_per_program))
                # todo: task requirement 3 - Specify which Windows versions or program versions are probably problematic (less the 85 percent success rate)
                self._csv_write_dict_section(
                    str_writer,
                    f"Operations with problematic success rate (below {Constants.problematic_threshold})",
                    lambda: self._csv_dict_writer(report_file, self._rates.problematic_versions))
                # todo: task requirement 4 - Specify Windows versions and program versions where the “check” function success rate doesn’t match the “install_plugin” success rate (+- 15%)
                str_writer.writerow(
                    [f"Check vs Install pluging operations success rate diff (above {Constants.diff_threshold})"])
                diffs_writer = csv.DictWriter(report_file, fieldnames=self._rates.diffs[0].keys())
                diffs_writer.writeheader()
                for diff_dict in self._rates.diffs:
                    diffs_writer.writerows([diff_dict])
            except Exception as ex:
                raise
