# Fun tool for versions execution and reporting
## what to expect:
1. for my insanity, num_of_runs per combination reduced to 10 instead of 500.
why? please see "exercise feedback" section below
2. readable and DRY code
3. simple SOLID design
4. testable components with few tests examples (mostly for dev)
5. minimal logging and error handling (just for better practice)
6. noob asyncio and multiprocessing
7. CI ready: dockerized env, both fun app and its tests
8. bugs

## what not to expect:
1. design diagram
2. pythonic code
3. proper asyncio usage
4. error handling & logging
5. generics, decorators, decorators
6. GIT & CI
7. UTs

## personal notes:
1. As a python noob, it was a great way to get familiar with python different features
2. It took me much more than 2 hours, more like 2 days. and it is still far from how I would like to see it.

## exercise feedback:
1. provided tool doesn't run on mac
2. 500 per each combination is too much. 
bash execution of 500 main runs takes 3 minutes, you ask to run 12000.
event with multiprocessing it is too much.
![img.png](img.png)
3. too big for h.w. over 2 hours task for any decent developer.
why?
   1. design
   2. code with good "manners" - naming, files/object/methods structure, logging, etc..
   3. check yourself - basic uts, runtimes
   4. code review yourself for "poop" or unused params
   5. etc..
4. you call this exercise "automatic test", I hope it is not your automation design

why?

get a large cup of coffee and free your schedule, here are few topics:
   1. requires manual analytics after the run
   2. not clear what is being tested
   3. S from SOLID applies to automation too, each test has single test responsibility
   4. report creation if part of automation infra, which is product (internal) by itself. 
   with roadmap, project management, versions, testing, and customers. 
   loosely couple to production code