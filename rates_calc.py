from constants import Constants


class RatesDto:
    _check_rates = {}
    _check_per_windows = {}
    _check_per_program = {}
    _install_plugin_rates = {}
    _install_plugin_per_windows = {}
    _install_plugin_per_program = {}
    _problematic_versions = {}
    _diffs = []

    def __init__(self, check_rates, check_per_windows, check_per_program,
                 install_plugin_rates, install_plugin_per_windows, install_plugin_per_program,
                 problematic_versions, diffs):
        self._check_rates = check_rates
        self._check_per_windows = check_per_windows
        self._check_per_program = check_per_program
        self._install_plugin_rates = install_plugin_rates
        self._install_plugin_per_windows = install_plugin_per_windows
        self._install_plugin_per_program = install_plugin_per_program
        self._problematic_versions = problematic_versions
        self._diffs = diffs

    @property
    def problematic_versions(self):
        return self._problematic_versions

    @property
    def check_rates(self):
        return self._check_rates

    @property
    def check_per_windows(self):
        return self._check_per_windows

    @property
    def check_per_program(self):
        return self._check_per_program

    @property
    def install_plugin_rates(self):
        return self._install_plugin_rates

    @property
    def install_plugin_per_windows(self):
        return self._install_plugin_per_windows

    @property
    def install_plugin_per_program(self):
        return self._install_plugin_per_program

    @property
    def diffs(self):
        return self._diffs


class RatesCalc:
    _problematic_versions = {}
    _check_rates = {}
    _install_plugin_rates = {}

    def add_check_program_run(self, win_version, program_version, successful_runs):
        self._check_rates = self._calc_rates_and_store(self._check_rates, win_version, program_version, successful_runs)

    def add_install_plugin_run(self, win_version, program_version, successful_runs):
        self._install_plugin_rates = self._calc_rates_and_store(self._install_plugin_rates, win_version, program_version, successful_runs)

    def get_calculated_rates(self) -> RatesDto:
        check_per_windows, check_per_program = self._calc_avg_rate_per_version(self._check_rates)
        install_plugin_per_windows, install_plugin_per_program = self._calc_avg_rate_per_version(self._install_plugin_rates)
        diffs = self._calc_diffs()

        return RatesDto(self._check_rates, check_per_windows, check_per_program,
                        self._install_plugin_rates, install_plugin_per_windows, install_plugin_per_program,
                        self._problematic_versions, diffs)

    def _calc_avg_rate_per_version(self, rates): #todo: -> tuple(dict, dict):
        avg_rates_per_windows = {}
        rates_per_program = {}
        avg_rates_per_program = {}
        rates_keys_len = len(rates.keys())

        for win_ver in rates:
            rates_sum = 0
            for prog_ver in rates[win_ver]:
                rate = rates[win_ver][prog_ver]
                rates_sum += rate
                if prog_ver not in rates_per_program:
                    rates_per_program[f"{prog_ver}"] = rate
                else:
                    rates_per_program[f"{prog_ver}"] += rate
            avg_rates_per_windows[f"{win_ver}"] = rates_sum / len(rates[win_ver].values())

        for prog_ver in rates_per_program:
            avg_rates_per_program[f"{prog_ver}"] = rates_per_program[f"{prog_ver}"] / rates_keys_len

        return avg_rates_per_windows, avg_rates_per_program

    def _calc_rates_and_store(self, rates, win_version, program_version, successful_runs):
        rate = successful_runs / Constants.num_of_runs
        self._handle_problematic_rate(win_version, program_version, rate)
        if win_version not in rates:
            rates[win_version] = {program_version: rate}
        else:
            rates[win_version].update({program_version: rate})
        return rates

    def _handle_problematic_rate(self, win_version, program_version, rate):
        if rate < Constants.problematic_threshold:
            self._problematic_versions[f"{win_version}_{program_version}"] = rate

    def _calc_diffs(self):
        diffs = []
        for win_ver in self._check_rates:
            for prog_ver in self._check_rates[win_ver]:
                check_prog_rate = self._check_rates[win_ver][prog_ver]
                install_plugin_prog_rate = self._install_plugin_rates[win_ver][prog_ver]
                rate_diff = abs(check_prog_rate - install_plugin_prog_rate)
                if rate_diff > Constants.diff_threshold:
                    diffs.append({
                        "windows_version": f"{win_ver}",
                        "programs_version": f"{prog_ver}",
                        "check_operation_rate": f"{check_prog_rate}",
                        "install_plugin_operation_rate": f"{install_plugin_prog_rate}",
                        "diff": f"{rate_diff}"
                    })
        return diffs
