import logging.handlers
from rates_calc import RatesCalc
from constants import Constants, CommandType
from utils import execute_cmd
from report_creator import ReportCreator
import asyncio
from multiprocessing import Pool, cpu_count
import time


class Fun:
    logfile_path = "{}.log".format(__file__)
    logging.basicConfig(level=logging.DEBUG,
                        format='[%(asctime)s - %(levelname)s]: %(message)s',
                        datefmt='%d.%m.%Y %H:%M:%S',
                        filename=logfile_path,
                        force=True)
    log = logging.getLogger(__name__)
    #todo: for debug redirect logger to stdout
    # log.addHandler(logging.StreamHandler())

    def main_flow(self):
        try:
            # todo: run_with_timer decorator
            tic = time.perf_counter()
            rates_calc = asyncio.run(self.get_scenario_rates())
            toc = time.perf_counter()
            self.log.debug(f"get_scenario_rates run time is: {toc - tic:0.4f} seconds")

            tic = time.perf_counter()
            calced_rates = rates_calc.get_calculated_rates()
            toc = time.perf_counter()
            self.log.debug(f"get_calculated_rates run time is: {toc - tic:0.4f} seconds")

            tic = time.perf_counter()
            ReportCreator(calced_rates).create_report()
            toc = time.perf_counter()
            self.log.debug(f"get_calculated_rates run time is: {toc - tic:0.4f} seconds")
        except Exception as ex:
            self.log.exception('got error')
            raise

    async def get_scenario_rates(self) -> RatesCalc:
        rates_calc = RatesCalc()

        async def run_windows_version(cmd_type, win_version):
            async def run_program_versions(cmd_type, program_version):
                if cmd_type == CommandType.CHECK:
                    cmd_operation = Constants.check_operation
                else:
                    cmd_operation = Constants.install_operation
                cmd = f'{Constants.exec_file} {cmd_operation} {win_version} {program_version}'

                async def exec_command_multiple_times():
                    cmd_list = []
                    for index in range(Constants.num_of_runs):
                        cmd_list.append([cmd])
                        await asyncio.sleep(0)
                    with Pool(cpu_count() - 1) as p:
                        results = p.starmap(execute_cmd, cmd_list)
                        p.close()
                        p.join()
                        await asyncio.sleep(0)
                    return sum(results)

                async def update_multiple_exec_results(ver_sum):
                    if cmd_type == CommandType.CHECK:
                        rates_calc.add_check_program_run(win_version, program_version, ver_sum)
                    else:
                        rates_calc.add_install_plugin_run(win_version, program_version, ver_sum)
                    await asyncio.sleep(0)

                ver_sum = await exec_command_multiple_times()
                await update_multiple_exec_results(ver_sum)
            await asyncio.gather(*(run_program_versions(cmd_type, program_version) for program_version in Constants.program_versions), return_exceptions=True)
        tasks = [*(run_windows_version(CommandType.CHECK, win_version) for win_version in Constants.windows_versions)]
        install_tasks = [*(run_windows_version(CommandType.INSTALL_PLUGIN, win_version) for win_version in Constants.windows_versions)]
        tasks.extend(install_tasks)
        await asyncio.gather(*tasks, return_exceptions=True)

        return rates_calc


if __name__ == "__main__":
    Fun().main_flow()