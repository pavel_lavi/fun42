import pytest
from fun import Fun
from report_creator import ReportCreator
from rates_calc import RatesDto
import time
import asyncio
from utils import execute_cmd
import logging.handlers
from multiprocessing import Pool, cpu_count

logfile_path = "{}.log".format(__file__)
logging.basicConfig(level=logging.DEBUG,
                    format='[%(asctime)s - %(levelname)s]: %(message)s',
                    datefmt='%d.%m.%Y %H:%M:%S',
                    filename=logfile_path,
                    force=True)
log = logging.getLogger(__name__)


# todo: for debug redirect logger to stdout
# log.addHandler(logging.StreamHandler())


def test_cmd_executor():
    try:
        params = []
        for index in range(500):
            params.append([f"./main check windows11 56"])
        tic = time.perf_counter()
        with Pool(cpu_count() - 1) as p:
            results = p.starmap(execute_cmd, params)
            p.close()
            p.join()
    except Exception as ex:
        raise
    toc = time.perf_counter()
    run_time = f"{toc - tic:0.4f}" # 500 214s
    assert True


def test_get_scenario_rates():
    tic = time.perf_counter()
    try:
        rates_calc = asyncio.run(Fun().get_scenario_rates())
    except Exception as ex:
        raise
    toc = time.perf_counter()
    run_time = f"{toc - tic:0.4f}"
    assert True


def test_report_creator():
    problematic = {'windows11_98': 0.5, 'windows10_98': 0.5, 'windows7_56': 0.5, 'windows7_98': 0.0}
    check = {'windows11': {'56': 1.0, '98': 0.5}, 'windows10': {'56': 1.0, '98': 1.0},
             'windows7': {'56': 1.0, '98': 0.5}}
    check_per_windows = {'windows11': 0.74, 'windows10': 0.99, 'windows7': 0.75}
    check_per_program = {'98': 0.88, '56': 1}
    install_plugin_per_windows = {'windows11': 0.64, 'windows10': 0.92, 'windows7': 0.78}
    install_plugin_per_program = {'98': 1, '56': 0.79}
    install = {'windows11': {'56': 1.0, '98': 1.0}, 'windows10': {'56': 1.0, '98': 0.5},
               'windows7': {'56': 0.5, '98': 0.0}}
    diffs = [{'windows_version': 'windows11', 'programs_version': '98', 'check_operation_rate': '0.5',
              'install_plugin_operation_rate': '1.0', 'diff': '0.5'},
             {'windows_version': 'windows10', 'programs_version': '98', 'check_operation_rate': '1.0',
              'install_plugin_operation_rate': '0.5', 'diff': '0.5'},
             {'windows_version': 'windows7', 'programs_version': '56', 'check_operation_rate': '1.0',
              'install_plugin_operation_rate': '0.5', 'diff': '0.5'},
             {'windows_version': 'windows7', 'programs_version': '98', 'check_operation_rate': '0.5',
              'install_plugin_operation_rate': '0.0', 'diff': '0.5'}]
    rates_dto = RatesDto(check, check_per_windows, check_per_program,
                         install, install_plugin_per_windows, install_plugin_per_program,
                         problematic, diffs)
    try:
        ReportCreator(rates_dto).create_report()
    except Exception as ex:
        raise
    assert True


def test_fun():
    tic = time.perf_counter()
    try:
        Fun().main_flow()
    except Exception as ex:
        raise
    toc = time.perf_counter()
    run_time = f"{toc - tic:0.4f}"
    assert True