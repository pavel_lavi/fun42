from enum import Enum


class Constants:
    # todo: conf file
    windows_versions = ["windows11", "windows10", "windows7", "windows_server_2016", "windows_server_2019", "windows_server_2012"]
    program_versions = ["56", "98", "74", "35"]
    exec_file = "./main"
    check_operation = "check"
    install_operation = "install_plugin"
    num_of_runs = 10  # todo: instead of 500
    problematic_threshold = 0.85
    diff_threshold = 0.15


class CommandType(Enum):
    CHECK = 1
    INSTALL_PLUGIN = 2
